import numpy as np
import matplotlib.pyplot as plt
#Importa la data
my_data = np.genfromtxt('datosh2.csv', delimiter=',')
r = my_data[0:,0]
e1 = my_data[0:,1]
e2 = my_data[0:,2]
j11 = my_data[0:,3]
j12 = my_data[0:,4]
j22 = my_data[0:,5]
k12 = my_data[0:,6]
h11 = e1 - j11
h22 = e2 - j22
#Defino las funciones
def f1(r,h11,j11):
    y = 2*h11 + j11 + 1/r
    return y
def f2(r,h22,j22):
    y = 2*h22 + j22 + 1/r
    return y
def f3(r,h11,h22,j12,k12):
    y = h11 + h22 + j12  - k12 + 1/r
    return y
def f4(r,h11,h22,j12):
    y = h11 + h22 + j12 + 1/r
    return y
#Ploteo
my_dpi=95
plt.figure(figsize=(850/my_dpi,500/my_dpi), dpi=my_dpi)
plt.axhline(y=0,linestyle='--',color='gray')
plt.plot(r,f1(r,h11,j11),'o-',label='11_')
plt.plot(r,f2(r,h22,j22),'o-',label='22_')
plt.plot(r,f3(r,h11,h22,j12,k12),'o-',label='12/1_2_')
plt.plot(r,f4(r,h11,h22,j12),'o-',label='12_/1_2')
plt.xlim([0.5,4])
plt.ylim([-1.5,2])
plt.xlabel('R')
plt.ylabel('Energy')
plt.legend()
plt.savefig(fname='estados.png',format='png',dpi=my_dpi)
